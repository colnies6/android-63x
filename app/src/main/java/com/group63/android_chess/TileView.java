package com.group63.android_chess;

/* Old code imported
 * @author Colin Nies & Ryan Shah
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;


public class TileView extends ImageView {
    public String currentPiece;
    public String label;

    public TileView(Context givenContext) {
        super(givenContext);
        this.currentPiece = "empty";
        this.label = "nowhere";
    }

    public TileView(Context givenContext, AttributeSet givenAttrs) {
        super(givenContext, givenAttrs);
        this.currentPiece = "empty";
        this.label = "nowhere";
    }

    public TileView(Context givenContext, AttributeSet givenAttrs, int givenStyleAttr) {
        super(givenContext, givenAttrs, givenStyleAttr);
        this.currentPiece = "empty";
        this.label = "nowhere";
    }

    public TileView(Context givenContext, AttributeSet givenAttrs, int givenStyleAttr, int givenStyleRes) {
        super(givenContext, givenAttrs, givenStyleAttr, givenStyleRes);
        this.currentPiece = "empty";
        this.label = "nowhere";
    }
}