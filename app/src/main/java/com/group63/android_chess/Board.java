package com.group63.android_chess;

/* Old code imported
* @author Colin Nies & Ryan Shah
 */

public class Board {
    public static Tile[][] tiles = new Tile[9][9];

    public static void initializeBoard() {
        for (int i = 8; i > -1; --i) {
            for (int j = 8; j > -1; --j) {
                tiles[i][j] = new Tile();

                //an ugly method for determining the tile file (column)
                //TODO: make this elegant
                char file = 96;
                switch (j) {
                    case 8:
                        file += 1;
                        break;
                    case 7:
                        file += 2;
                        break;
                    case 6:
                        file += 3;
                        break;
                    case 5:
                        file += 4;
                        break;
                    case 4:
                        file += 5;
                        break;
                    case 3:
                        file += 6;
                        break;
                    case 2:
                        file += 7;
                        break;
                    case 1:
                        file += 8;
                        break;
                    case 0:
                        file = '|';
                        break;
                }
                tiles[i][j].label = Character.toString(file) + Integer.toString(i);

                /* White tiles have the property of both i and j having the same parity (odd or even)
                 *  so if i + j is divisible by 2, then this is a white tile. Else it is a black tile (which has been set up by default).*/
                if ((i + j) % 2 == 0)
                    tiles[i][j].defColor = "     ";
            }
        }

        tiles[1][1].currPiece = "wR";
        tiles[1][8].currPiece = "wR";
        tiles[8][1].currPiece = "bR";
        tiles[8][8].currPiece = "bR";

        tiles[1][2].currPiece = "wN";
        tiles[1][7].currPiece = "wN";
        tiles[8][2].currPiece = "bN";
        tiles[8][7].currPiece = "bN";

        tiles[1][3].currPiece = "wB";
        tiles[1][6].currPiece = "wB";
        tiles[8][3].currPiece = "bB";
        tiles[8][6].currPiece = "bB";

        tiles[1][5].currPiece = "wQ";
        tiles[1][4].currPiece = "wK";
        tiles[8][5].currPiece = "bQ";
        tiles[8][4].currPiece = "bK";

        for (int x = 1; x < 9; x++) {
            tiles[2][x].currPiece = "wp";
        }
        for (int x = 1; x < 9; x++) {
            tiles[7][x].currPiece = "bp";
        }
    }
}