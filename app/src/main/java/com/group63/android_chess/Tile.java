package com.group63.android_chess;

/* Old code imported
 * @author Colin Nies & Ryan Shah
 */

public class Tile {
    public String currPiece;
    public String defColor;
    public String label;

    public Tile(){
        this.currPiece="empty";
        this.defColor="## ";
        this.label="nowhere";
    }
}